## Quiz on Objectives { data-transition="convex-in slide-out" }

* VOR uses the VHF frequency band, which is "line of sight" &mdash; what does this mean?
* What workflow do we use to begin using a VOR ground station?
* What are some means by which to find information on VOR ground stations?
* An onboard VOR indicator reads 230 TO &mdash; which radial is it on?

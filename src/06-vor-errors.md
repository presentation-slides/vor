## VOR Errors { data-transition="zoom-in slide-out" }

---

## VOR Errors { data-transition="fade-in slide-out" }

* Although the AIP specifies a Rated Coverage for a VOR, a specific VOR may be subject to limitations due to errors
* These limitations are detailed in the ERSA
* For example, Sunshine Coast Airport VOR
  * Coverage reduced in minor arc 169 to 340 due terrain shielding
  * Bending and scalloping on 224 radial at low altitudes

---

## VOR Errors { data-transition="fade-in slide-out" }

###### Ground Station Error

* The accuracy and strength of the signal from a VOR ground station can be affected by equipment
  * radio transmitter
  * radio antennae
  * power supply
* This may cause an error of accuracy up to 2&deg;

---

## VOR Errors { data-transition="fade-in slide-out" }

###### Site Effect Error

* The propagation path of VOR radio signals may be affected by physical obstacles located nearby the transmitter site
  * buildings
  * fences
  * terrain
* These errors are checked when the station is first commissioned, then periodically thereafter
* Site Errors are typically not more than 3&deg;
* However, if large errors are unavoidable, they are published in the ERSA or if temporary, by NOTAM

---

## VOR Errors { data-transition="fade-in slide-out" }

###### Terrain Effect Error

* The VOR signal may be distorted by signals reflected by terrain between the radio transmitter and the receiver onboard the aircraft
* This type of error causes oscillations of the Course Deviation Indicator (CDI) of the VOR instrument onboard the aircraft
  * Rapid oscillations are called **scalloping**
  * Slow oscillations are called **course bending**
* Terrain Errors are typically not more than 2&deg;
* However, more significant errors will be published in the ERSA

---

## VOR Errors { data-transition="fade-in slide-out" }

###### Airborne Equipment Error

* Errors may arise due to imperfections of the airborne equipment
* Airborne Equipment Errors are typically not more than 2&deg;

---

## VOR Errors { data-transition="fade-in slide-out" }

###### Vertical Polarisation Error

* VOR ground station emit radio waves which are oriented horizontally, parallel to the Earth's surface
* An aircraft's VOR antenna is sensitive to these _horizontally polarised signals_
* Radio signals may be reflected from large obstacles such that they become vertically oriented radio waves
* These signals will only be received if the aircraft is banked and the VOR antenna is moved away from its horizontal orientation
* Vertical Polarisation Error will be apparent as large, rapid deflections of the CDI during a turn, that stop when returned to level flight
* This type of error is generally rare

---

## VOR Errors { data-transition="fade-in slide-out" }

###### Aggregate Error

* The combined effect of all sources of error to a VOR indication is the _aggregate error_
* The magnitude of error will vary and is difficult to precisely determine
* Under typical circumstances, the aggregate error rarely exceeds 5&deg;

---


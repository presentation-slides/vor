## What is a VOR { data-transition="zoom-in slide-out" }

---

## What is a VOR { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  A VHF Omni-directional Radio (VOR) is:
</p>

* A ground-based radio station used for navigating our aircraft, using our onboard navigation aids
* Operates in the VHF radio frequency range 108-118 MHz, with 160 individual channels, spaced at 50KHz
* The term _omni_ refers to the VOR ground station radiating an infinite number of tracks in all directions
* For practical purposes, we use 360 parts of this radio signal, each separated at 1&deg;
* These are called _radials_ and are relative to **magnetic north**

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vor-radials.png" alt="VOR radials" width="300px"/>
</p>

---

## What is a VOR { data-transition="fade-in slide-out" }

* a VOR ground station transmits two VHF radio signals
  1. a **reference phase signal** which is transmitted in all directions _(omni-directional)_
  2. a **variable phase signal** which rotates uniformly through each radial
* the phases misalign by some amount on all other radials, with full misalignment on the 180&deg; radial
* by measuring the difference in phase alignment, **a VOR receiver can determine which radial it is located on**

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vor-phase-alignment.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="VOR phase alignment" width="380px"/>
</p>

---

## What is a VOR { data-transition="fade-in slide-out" }

* a VOR ground station also transmits a Morse-code identifying signal
* sometimes a VOR is undergoing maintenance, and will transmit "XP" (<code>-&#xB7;&#xB7;- &#xB7;--&#xB7;</code>) indicating that it is unusable
* a VOR ground station may also have limited voice communication facilities e.g. ATIS

---

## What is a VOR { data-transition="fade-in slide-out" }

<p style="font-size: x-large; text-align: center">
  The Sunshine Coast Airport VOR on the Bundaberg VNC
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/ybsu-chart-vor.png" alt="YBSU Chart VOR" width="400px"/>
</p>

* Note:
  * The Sunshine Coast VOR location
  * The Sunshine Coast VOR frequency **114.2 MHz**
  * The Sunshine Coast VOR identifying code <b><code>&#xB7;&#xB7;&#xB7; &#xB7;&#xB7;-</code></b> (SU)

---

## What is a VOR { data-transition="fade-in slide-out" }

<p style="font-size: x-large; text-align: center">
  The Sunshine Coast Airport and Brisbane Airport VORs on the En Route Chart (ERC)
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/ercl3-vor.png" alt="ERCL3 VOR" width="600px"/>
</p>

---

## What is a VOR { data-transition="fade-in slide-out" }

<p style="font-size: x-large; text-align: center">
  The ERSA also has details on navigation aids e.g. the VOR at Brisbane Airport
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/ybbn-ersa-navaids.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="YBBN ERSA Navaids" width="800px"/>
</p>

* Note:
  * The identifier for the VOR **(BN)**
  * The frequency for the VOR **(113.2 MHz)**
  * The latitude/longitude of the VOR **(272157.5S 1530821.4E)**
  * Footnote **(1)**: the ARP is located at a bearing of **216&deg;** and **1.6 NM** from the VOR


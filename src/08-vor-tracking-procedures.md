## VOR Tracking Procedures { data-transition="zoom-in slide-out" }

---

## VOR Tracking Procedures { data-transition="fade-in slide-out" }

###### Tracking TO a VOR Ground Station

* Turn the OBS until the CDI is centred and the Indication Flag reads TO
* Read off the bearing under the Course Index
* Set the aircraft on that **track**
* Remember to take into account drift angle when setting a heading

---

## VOR Tracking Procedures { data-transition="fade-in slide-out" }

###### Tracking TO a VOR Ground Station

* For example, an aircraft positioned on the <span class="fragment highlight-red">red arrow</span> sets track of 050&deg; to the VOR ground station
* An aircraft positioned on the <span style="color: #7030a0">purple arrow</span> sets track of 230&deg; to the VOR ground station

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vor-radial.png" alt="Aircraft on VOR radial" width="300px"/>
</p>

---

## VOR Tracking Procedures { data-transition="fade-in slide-out" }

###### Tracking Abeam a VOR Ground Station

* It is common to use a VOR ground station to monitor progress of a flight and note the time when passing abeam
* Determine the current track of the aircraft
* Turn the OBS so that the Course Index reads 90&deg; to the aircraft track
* If the aircraft is not within 10&deg; abeam the VOR, the CDI will be fully deflected to one side
* As the aircraft passes to within 10&deg; of the VOR ground station, the CDI will begin to move toward the centre
* When the CDI is centred, the aircraft is abeam the VOR ground station &mdash; note the time
* The CDI will deflect in the opposite direction until the aircraft is outside of 10&deg; of the VOR ground station

---

## VOR Tracking Procedures { data-transition="fade-in slide-out" }

###### Tracking Abeam a VOR Ground Station

* For example, for an aircraft on a track of 010&deg;, turn the OBS to read 100 under the Course Index
* If the VOR ground station is to the left of intended track, the CDI will be fully deflected to the left, and vice versa
* As the aircraft continues on a track of 010&deg;, the CDI will centre on the 100 radial &mdash; the aircraft is abeam the VOR ground station
* Continuing on track, the CDI will deflect to the right

---

## VOR Tracking Procedures { data-transition="fade-in slide-out" }

###### Passing Overhead a VOR Ground Station

* With the CDI centred and the Indicator Flag reading TO, track toward a VOR ground station
* As the aircraft comes closer to the station, the CDI will become more sensitive and oscillate
* During a brief time overhead the station, the Indicator Flag will switch to OFF
* This area is called the _cone of confusion_ and is dependent on the height of the aircraft above the station &mdash; the higher, the large will be the "cone"
* After passing overhead the station, and leaving the cone confusion, the Indicator Flag will read FROM
* The CDI oscillation will further stabilise as the aircraft flies further from the station

---

## VOR Tracking Procedures { data-transition="fade-in slide-out" }

###### Intercepting a Track Using a VOR Ground Station

* To intercept a track, you must first determine:
  1. Where you are
  2. Where you want to go
  3. How you get there
* **Where you are:** Turn the OBS until the CDI is centred and the Indication Flag reads FROM &mdash; you are on this radial from the VOR station
* **Where you want to go:** Turn the OBS to the planned track to or from the VOR ground station and confirm the Indicator Flag reads correctly (TO/FROM)
* **How you get there**: Set the intended aircraft track before intercepting the planned track

---

## VOR Tracking Procedures { data-transition="fade-in slide-out" }

###### Intercepting a Tracking Using a VOR Ground Station

* Initially, the CDI will be deflect to one side
* As the aircraft tracks closer to the intended intercept track, the CDI will move toward the centre
* Prior to the CDI reaching centre, start to turn on to the intended track so that the aircraft completes the turn with the CDI centred
* Select a heading for the intercept that accounts for any crosswind

---

## VOR Tracking Procedures { data-transition="fade-in slide-out" }

###### Intercepting a Tracking Using a VOR Ground Station

* For example, while heading 198&deg;M, you intended to track inbound on the 190 radial to a VOR ground station. The wind is westerly.
* Turn the OBS so that the CDI centres &mdash; you determine you are on the 230 radial
* Given a 90&deg; intercept to the 190 radial, the aircraft track to the intercept will be 100&deg;M
* Turn the OBS to the 190 radial with the Indicator Flag showing FROM
* The track to the VOR will be 010&deg;M, with a wind correction of approximately 5&deg; to the left (HDG 005&deg;M)
* As the CDI begins to centre, start turning on to the intended inbound track
* When executed accurately, rolling out of the turn, the CDI will be centred on the 190 radial

---


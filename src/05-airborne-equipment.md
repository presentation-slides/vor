## Airborne VOR Equipment { data-transition="zoom-in slide-out" }

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

* The airborne VOR equipment consists of:
  * The VOR receiving antenna &mdash; typically V-shaped to receive VHF frequencies 108-118 MHz
  * The VOR receiving radio &mdash; typically combined with the COM radio into what we call the _VHF NAV-COM_ set
  * One or more VOR indicators to interpret the received signals into a display to the pilot

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

###### VOR Receiving Antenna

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/cessna-vor-antenna.jpg" alt="Cessna VOR antenna" width="400px"/>
</p>

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

###### VHF NAV-COM 1 and 2 onboard Cessna 172 VH-OCP

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vhf-navcom.png" alt="VH-OCP VHF NAV-COM" width="400px"/>
</p>

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

###### VOR indicators 1 and 2 onboard Cessna 172 VH-OCP

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-1-2.png" alt="VH-OCP VOR" width="900px"/>
</p>

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

###### VOR indicators 1 and 2 onboard Cessna 172 VH-OCP

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-1-2b.png" alt="VH-OCP VOR" width="350px"/>
</p>

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  OBS Course Card
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-indicator-obs-course-card.png" alt="VH-OCP OBS Course Card" width="350px"/>
</p>

* The OBS Course Card turns by turning the Omni-Bearing Selector (OBS)
* The set course is the one that falls under the Course Index
* _Note: the term "course" is not used in Australia for navigation &mdash; it means the same as "track"_

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  Course Index
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-indicator-course-index.png" alt="VH-OCP Course Index" width="350px"/>
</p>

* The VOR Course Index is the currently configured radial
* It can be adjusted by turning the OBS
* This VOR is set to the 355 radial under the Course Index

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  Omni-Bearing Selector (OBS)
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-indicator-obs.png" alt="VH-OCP OBS" width="350px"/>
</p>

* By turning the OBS, the OBS Course Card will turn
* The configured radial can be read from the Course Index

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  Course Deviation Indicator (CDI)
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-indicator-cdi.png" alt="VH-OCP VOR CDI" width="350px"/>
</p>

* The CDI will be centred or deflected to the left/right, depending on:
  * which VOR radial the aircraft is positioned on
  * which radial is configured by the OBS

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  Deviation Scale
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-indicator-deviation-scale.png" alt="VH-OCP VOR Deviation Scale" width="350px"/>
</p>

<p style="font-size: x-small; text-align: left">
  <ul style="font-size: medium; text-align: left">
    <li>The Deviation Scale indicates how far the aircraft has deviated from the configured radial on the CDI</li>
    <li>Each dot represents 2&deg; of deviation, with a maximum deflection of 10&deg;</li>
    <li>
      For example, if:
      <ul>
        <li>the CDI is positioned to the left of centre by two dots</li>
        <li>the Course Index is configured on the 355 radial</li>
        <li>the Indicator Flag indicates "FROM"</li>
      </ul>
    </li>
    <li>
      then:
      <ul>
        <li>the aircraft is 4&deg; to the right of the 355 radial</li>
        <li>it is on the 351 radial</li>
      </ul>
    </li>
  </ul>
</p>

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  Indicator Flag
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-indicator-nav-flag.png" alt="VH-OCP Indicator Flag" width="350px"/>
</p>

* The Indicator Flag will read one of
  * **TO**: we are tracking to the VOR on the configured radial
  * **FROM**: we are tracking from the VOR on the configured radial
  * **_OFF_**: the indication on the VOR is not reliable

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  Glide Slope Indicator
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-indicator-gsi.png" alt="VH-OCP Glide Slope Indicator" width="350px"/>
</p>

* Some VORs are fitted with a Glide Slope Indicator and Indication Flag
* _This indicator is not a component of this briefing_

---

## Airborne VOR Equipment { data-transition="fade-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  The VOR Indication is Independent of our Heading
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-indicator.png" alt="VH-OCP Omni-Bearing Indicator" width="350px"/>
</p>

* It is important to understand that the VOR Indication gives us a position on a VOR radial
* It is **independent of our aircraft heading**
* If our aircraft were in the same position, but on a different heading, the VOR indication would always be the same

---


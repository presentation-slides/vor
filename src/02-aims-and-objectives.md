## Aims and Objectives { data-transition="zoom-in slide-out" }

###### Aims { data-transition="fade-in slide-out" }

* to learn about VOR equipment based, both on the ground and airborne
* to learn how to use VOR equipment to navigate an aircraft

---

## Aims and Objectives { data-transition="fade-in slide-out" }

###### Objectives { data-transition="fade-in slide-out" }

* understand the navigational purpose of VOR
* understand how VOR radio transmitters and receivers function
* obtain information on VOR ground transmitting stations
* operate and interpret airborne VOR equipment
* identify and correct for errors in VOR indications
* obtain the fundamental skills to navigate an aircraft to/from a VOR
* develop the onboard workflows associated with the operation of a VOR

---


## VOR Setup Procedures { data-transition="zoom-in slide-out" }

---

## VOR Setup Procedures { data-transition="fade-in slide-out" }

###### To set up the onboard VOR indicator, we follow a workflow: T.I.T.

1. **T**une
2. **I**dentify
3. **T**est

---

## VOR Setup Procedures { data-transition="fade-in slide-out" }

###### Tune

1. Set the desired VOR frequency on the NAV radio
2. Press the flip-flop button to toggle the frequency from STANDBY to ACTIVE

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vor-tune.png" alt="VH-OCP NAV Radio Tune" width="550px"/>
</p>

---

## VOR Setup Procedures { data-transition="fade-in slide-out" }

###### Identify

1. Press the desired NAV button on the NAV-COM control unit
2. This will allow you to hear the Morse Code for the VOR ground station
3. Confirm that you are hearing the correct station to positively identify it

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vhocp-vhf-navcom-nav1.png" alt="VH-OCP NAV Radio Identify" width="550px"/>
</p>

---

## VOR Setup Procedures { data-transition="fade-in slide-out" }

###### Test

1. Turn the OBS until the CDI is centred and the Indication Flag reads FROM
2. Take note of the Course Index to determine which radial you are on
3. Turn the OBS 4&deg; from the current radial
4. Confirm that the CDI deflects appropriately
5. Turn the OBS in the opposite direction approximately 20&deg;
6. Confirm that the CDI has maximum deflection in the opposite direction
7. Turn the OBS to return the CDI to the centre
8. If the CDI deflects as expected, we have positively tested operation of the VOR

---


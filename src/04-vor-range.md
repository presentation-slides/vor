## VOR Range { data-transition="zoom-in slide-out" }

---

## VOR Range { data-transition="fade-in slide-out" }

* Like other VHF radio e.g. communication, the signal is primarily determined by **line of sight**
* Therefore, the primary considerations for the maximum range of a VOR are:
  * the altitude of the aircraft receiving the VOR signal
  * the height of the transmitting antenna of the VOR ground station
  * the height of the terrain between the VOR ground station and the receiving station
  * VOR _errors_

---

<p style="font-size: x-large; font-weight: bold; text-align: center">
  AIP GEN 1.5 specifies Rated Coverage of a VOR for planning purposes
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/vor/vor-range-aip-gen-1.5.png" style="border: 2px solid #dcdcdc; padding: 10px" alt="VOR range AIP GEN 1.5" width="800px"/>
</p>

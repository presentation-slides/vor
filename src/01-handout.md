## Handout { data-transition="zoom-in slide-out" }

<p style="font-size: x-large; font-weight: bold; text-align: center">
  <a href="https://system-f.gitlab.io/training/handouts/vor.pdf">
    Link to Handout
    <br>
    <img src="https://system-f.gitlab.io/training/handouts/vor.png" width="400" alt="VOR handout"/>
  </a>
</p>

---


## Airmanship/TEM/HF { data-transition="convex-in slide-out" }

<section>
  <table>
      <thead>
        <tr>
            <th>Threat</th>
            <th>Error</th>
            <th>Management</th>
            <th>Mitigation</th>
        </tr>
      </thead>
      <tbody>
        <tr style="font-size: x-large">
          <td>Disorientation due to reciprocal VOR radial</td>
          <td>Failure to correctly interpret the TO/FROM Indicator Flag</td>
          <td>Interpret a VOR reading including the Indicator Flag e.g. "FROM 290 radial"</td>
          <td>Positively identify which radial the aircraft is positioned on</td>
        </tr>
        <tr style="font-size: x-large">
          <td>Loss of control or SA due to focus on navigation instruments</td>
          <td>Loss of management of aircraft state</td>
          <td>Recognise that aircraft state is of equal/higher priority than navigation "Aviate, Navigate, Communicate"</td>
          <td>Proactively include scan of horizon</td>
        </tr>
      </tbody>
    </table>
</section>

---

